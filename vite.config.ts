import type { UserConfig, ConfigEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
// 如果编辑器提示 path 模块找不到，则可以安装一下 @types/node -> npm i @types/node -D
import { resolve } from 'path'
import { loadEnv } from 'vite'
import styleImport from 'vite-plugin-style-import'

import { wrapperEnv } from './build/util'

/**
 * @description 获取当前目录
 */
const root: string = process.cwd()
/**
 * https://vitejs.dev/config/
 * @interface ConfigEnv  ==>  vite 中的接口定义
 * @param command 用来判断是打包build 还是本地启用serve
 * @param mode 用来判断模式是 development / test / production
 */

export default ({ command, mode }: ConfigEnv): UserConfig => {
  const env = loadEnv(mode, root)
  const viteEnv = wrapperEnv(env)
  const { VITE_PORT } = viteEnv
  const isBuild = command === 'build'
  console.log(isBuild)

  return {
    root,
    base: './',
    plugins: [
      vue(),
      styleImport({
        libs: [
          {
            libraryName: 'ant-design-vue',
            esModule: true,
            resolveStyle: (name) => {
              return `ant-design-vue/es/${name}/style/index`
            }
          }
        ]
      })
    ],
    resolve: {
      alias: {
        '@': resolve(__dirname, 'src')
      }
    },
    server: {
      port: VITE_PORT
    },
    css: {
      preprocessorOptions: {
        less: {
          javascriptEnabled: true
        }
      }
    }
  }
}
