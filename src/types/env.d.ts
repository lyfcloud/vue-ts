/*
 * @Author: 李一番
 * @Date: 2021-05-23 13:41:20
 * @Last Modified by: 李一番
 * @Last Modified time: 2021-05-23 13:43:01
 */

// 配置环境变量的只能提示
export interface ImportMetaEnv {
  VITE_PORT: string
  VITE_GLOB_APP_SHORT_NAME: string
  VITE_GLOB_NAME: string
  VITE_DROP_CONSOLE: boolean
}
