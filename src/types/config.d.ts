/*
 * @Author: 李一番
 * @Date: 2021-05-20 22:13:38
 * @Last Modified by: 李一番
 * @Last Modified time: 2021-05-23 14:03:04
 */
import { CacheTypeEnum } from '@/enums/cacheEnum'

export interface ProjectConfig {
  permissionCacheType: CacheTypeEnum
  globAppShortName: string
  defaultCacheTime: number
}
