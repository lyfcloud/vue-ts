/*
 * @Author: 李一番
 * @Date: 2021-05-20 23:05:13
 * @Last Modified by: 李一番
 * @Last Modified time: 2021-05-20 23:37:30
 */

declare type Nullable<T> = T | null
