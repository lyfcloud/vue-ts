import Axios from 'axios'
import { message } from 'ant-design-vue'

const baseURL = 'https://www.baidu.com'
const axios = Axios.create({
  baseURL,
  timeout: 20000
})

// 请求拦截
axios.interceptors.request.use(
  (response) => {
    // 可以根据请求数据进行处理
    return response
  },
  (error) => {
    return Promise.reject(error)
  }
)

// 响应拦截
axios.interceptors.response.use(
  (response) => {
    // 对返回数据进行统一处理
    return response
  },
  (error) => {
    return Promise.reject(error)
  }
)
