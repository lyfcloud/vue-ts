/*
 * @Author: 李一番
 * @Date: 2021-05-22 20:27:51
 * @Last Modified by: 李一番
 * @Last Modified time: 2021-05-22 20:36:06
 */
const { toString } = Object.prototype

export function isDef<T = unknown>(val?: T): val is T {
  return typeof val !== 'undefined'
}

export function isUnDef<T = unknown>(val?: T): val is T {
  return !isDef(val)
}

export function isNull(val: unknown): val is null {
  return val === null
}

export function isNullOrUndef(val: unknown): val is null | undefined {
  return isUnDef(val) || isNull(val)
}
