## 前端加密、解密流程

#### 1. 安装

```js
npm install -S crypto-js
npm install -D @types/crypto-js
```

#### 2. 注意事项

- 需要在 tsconfig.json 中加入配置

  ```json
  {
    "compilerOptions": {
      ...
      "skipLibCheck": true, // 启用它会阻止Typescript对整个导入的库进行类型检查，意味着只要您不使用导入库的不兼容部分，它们就可以正常编译
      ...
    }
  }
  ```

#### 3. 配置

```ts
// 配置加密参数
// 设置AES加密 16个字节
export const cacheCipher = {
  key: '1111111111111111',
  iv: '2222222222222222'
}
```

```ts
/**
 * @description 加密存储
 */
import { encrypt, decrypt } from 'crypto-js/aes'
import UTF8, { parse } from 'crypto-js/enc-utf8'
import ECB from 'crypto-js/mode-ecb'
import pkcs7 from 'crypto-js/pad-pkcs7'

export interface EncryptionParams {
  key: string
  iv: string
}

export class AesEncryption {
  private key // 加密使用的key
  private iv
  constructor(opt: EncryptionParams) {
    const { key, iv } = opt
    this.key = parse(key)
    this.iv = parse(iv)
  }
  get getOption() {
    return {
      mode: ECB, // 加密方式
      padding: pkcs7, // 填充方式
      iv: this.iv // 偏移量
    }
  }

  /**
   * @description AES加密
   * @param str 加密前数据
   * @returns 加密过后数据
   */
  encryptByAES(str: string) {
    const encrypted = encrypt(str, this.key, this.getOption)
    return encrypted.toString()
  }

  /**
   * @description AES解密
   * @param str 加密后数据
   * @returns 加密前数据
   */
  decryptByAES(str: string) {
    const encrypted = decrypt(str, this.key, this.getOption)
    return encrypted.toString(UTF8)
  }
}
```

```ts
// 使用
const a = new AesEncryption(cacheCipher)
const str = '111111111111111111111111111'
const a1 = a.encryptByAES(str)
const b1 = a.decryptByAES(a1)
console.log(str)
console.log(a1)
console.log(b1)
```
