/*
 * @Author: 李一番
 * @Date: 2021-05-20 21:24:30
 * @Last Modified by: 李一番
 * @Last Modified time: 2021-05-23 19:37:45
 */

import {
  setLocal,
  setSession,
  getLocal,
  getSession,
  removeLocal,
  clearLocal,
  removeSession,
  clearSession,
  BasicKeys
} from '@/utils/cache/persistent'
import projectSetting from '@/settings/projectSetting'
import { CacheTypeEnum } from '@/enums/cacheEnum'

const { permissionCacheType } = projectSetting

const isLocal = permissionCacheType === CacheTypeEnum.LOCAL

// 获取存储
export function getAuthCache(key: BasicKeys) {
  const fn = isLocal ? getLocal : getSession
  return fn(key)
}

// 设置存储
export function setAuthCache(key: BasicKeys, value: any) {
  const fn = isLocal ? setLocal : setSession
  return fn(key, value)
}

// 删除存储
export function removeAuthCache(key: BasicKeys) {
  const fn = isLocal ? removeLocal : removeSession
  return fn(key)
}

// 删除所有
export function clearAuthCache() {
  const fn = isLocal ? clearLocal : clearSession
  return fn()
}
