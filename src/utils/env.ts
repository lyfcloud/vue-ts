/**
 * @description 是否是开发模式
 */
export function isDevMode(): boolean {
  return import.meta.env.DEV
}

/**
 * @description 是否是生产模式
 */
export function isProdMode(): boolean {
  return import.meta.env.PROD
}

/**
 * @description 返回当前是哪种模式
 * @returns string development | production
 */
export function getEnv(): string {
  return import.meta.env.MODE
}
