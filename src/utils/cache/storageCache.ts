/*
 * @Author: 李一番
 * @Date: 2021-05-21 00:06:25
 * @Last Modified by: 李一番
 * @Last Modified time: 2021-05-23 15:13:14
 */

import { cacheCipher } from '@/settings/encryptionSetting'
import { isDevMode } from '@/utils/env'
import { AesEncryption, EncryptionParams } from '@/utils/encryption'
import { isNullOrUndef } from '@/utils/is'

export interface CreateStorageParams extends EncryptionParams {
  prefixKey: string
  storage: Storage
  hasEncrypt: boolean
  timeout?: number | null
}

export const createStorage = ({
  prefixKey = '',
  storage = sessionStorage,
  key = cacheCipher.key,
  iv = cacheCipher.iv,
  timeout = null,
  hasEncrypt = true
}: Partial<CreateStorageParams> = {}) => {
  if (hasEncrypt && [key.length, iv.length].some((item) => item !== 16)) {
    throw new Error('当加密时, key和iv必须为16个字节')
  }

  const encrytion = new AesEncryption({ key, iv })

  const WebStorage = class WebStorage {
    private storage: Storage
    private prefixKey?: string
    private encrytion: AesEncryption
    private hasEncrypt: boolean

    constructor() {
      this.storage = storage
      this.prefixKey = prefixKey
      this.encrytion = encrytion
      this.hasEncrypt = hasEncrypt
    }

    // 获取Key值
    private getKey(key: string) {
      return `${this.prefixKey}${key}`.toUpperCase()
    }

    /**
     * @description 设置存储
     * @param key 存储属性名
     * @param value 存储属性值
     * @param expire 过期时间段
     */
    set(key: string, value: any, expire: number | null = timeout) {
      const stringData = JSON.stringify({
        value,
        time: new Date(),
        expire: !isNullOrUndef(expire) ? new Date().getTime() + expire * 1000 : null
      })
      const stringValue = this.hasEncrypt ? this.encrytion.encryptByAES(stringData) : stringData
      this.storage.setItem(this.getKey(key), stringValue)
    }

    /**
     * @description 获取存储
     * @param key 存储属性名
     * @returns 存储值 或 null
     */
    get(key: string, def: null = null): any | void {
      const val = this.storage.getItem(this.getKey(key))
      if (!val) return def
      try {
        const decData = this.hasEncrypt ? this.encrytion.decryptByAES(val) : val
        const data = JSON.parse(decData)
        const { value, expire } = data
        if (isNullOrUndef(expire) || expire >= new Date().getTime()) {
          return value
        }
        this.remove(key)
      } catch (error) {
        return def
      }
    }

    /**
     * @description 删除某一存储
     * @param key 存储属性名
     */
    remove(key: string): void {
      this.storage.removeItem(this.getKey(key))
    }

    /**
     * @description 清除所有存储
     */
    clear(): void {
      this.storage.clear()
    }
  }

  return new WebStorage()
}
