/*
 * @Author: 李一番
 * @Date: 2021-05-20 23:40:44
 * @Last Modified by: 李一番
 * @Last Modified time: 2021-05-21 00:04:25
 */

export interface Cache<V = any> {
  value?: V
  timeoutId?: ReturnType<typeof setTimeout>
  time?: number
  alive?: number
}

const NOT_ALIVE = 0

export class Memory<T = any, V = any> {
  private cache: { [key in keyof T]?: Cache<V> } = {}

  private alive: number

  constructor(alive = NOT_ALIVE) {
    // Unit second
    this.alive = alive * 1000
  }

  get getCache() {
    return this.cache
  }

  setCache(cache: any) {
    this.cache = cache
  }

  get<K extends keyof T>(key: K) {
    return this.cache[key]
  }
}
