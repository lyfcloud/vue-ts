/*
 * @Author: 李一番
 * @Date: 2021-05-21 00:05:35
 * @Last Modified by: 李一番
 * @Last Modified time: 2021-05-23 14:10:18
 */

import projectSetting from '@/settings/projectSetting'
import { createStorage as create, CreateStorageParams } from '@/utils/cache/storageCache'
import { isProdMode } from '@/utils/env'

const { globAppShortName, defaultCacheTime } = projectSetting

export type Options = Partial<CreateStorageParams>

const createOptions = (storage: Storage, options: Options = {}): Options => {
  return {
    hasEncrypt: isProdMode(),
    storage,
    prefixKey: globAppShortName,
    ...options
  }
}

export const WebStorage = create(createOptions(sessionStorage))

export const createStorage = (storage: Storage = sessionStorage, options: Options = {}) => {
  return create(createOptions(storage, options))
}

export const createSessionStorage = (options: Options = {}) => {
  return createStorage(sessionStorage, { ...options, timeout: defaultCacheTime })
}

export const createLocalStorage = (options: Options = {}) => {
  return createStorage(localStorage, { ...options, timeout: defaultCacheTime })
}

export default WebStorage
