/*
 * @Author: 李一番
 * @Date: 2021-05-20 21:41:48
 * @Last Modified by: 李一番
 * @Last Modified time: 2021-05-23 19:33:08
 */

import { TOKEN_KEY, BASE_LOCAL_CACHE_KEY, BASE_SESSION_CACHE_KEY } from '@/enums/cacheEnum'
import { createSessionStorage, createLocalStorage } from '@/utils/cache'

interface BasicStore {
  [TOKEN_KEY]: string | number | undefined
}
type LocalStore = BasicStore
type SessionStore = BasicStore

export type BasicKeys = keyof BasicStore

const ls = createLocalStorage()
const ss = createSessionStorage()

interface CacheStore {
  local: { [x: string]: any }
  session: { [x: string]: any }
}

const cacheStore: CacheStore = {
  local: {},
  session: {}
}

export function initCache() {
  cacheStore.local = ls.get(BASE_LOCAL_CACHE_KEY) || {}
  cacheStore.session = ss.get(BASE_SESSION_CACHE_KEY) || {}
}
initCache()

/**
 * 获取本地存储值
 * @param key 属性名
 * @returns null | string
 */
export function getLocal(key: BasicKeys) {
  try {
    return cacheStore.local[BASE_LOCAL_CACHE_KEY][key]
  } catch (error) {
    return null
  }
}

/**
 * @description 设置本地存储
 * @param key 属性名
 * @param value 属性值
 */
export function setLocal(key: BasicKeys, value: BasicStore[BasicKeys]): void {
  const local = ls.get(BASE_LOCAL_CACHE_KEY)?.[BASE_LOCAL_CACHE_KEY] || {}
  cacheStore.local[BASE_LOCAL_CACHE_KEY] =
    {
      ...local,
      ...cacheStore.local[BASE_LOCAL_CACHE_KEY]
    } || {}

  cacheStore.local[BASE_LOCAL_CACHE_KEY][key] = value
  ls.set(BASE_LOCAL_CACHE_KEY, cacheStore.local)
}

/**
 * @description 删除某一个本地存储
 * @param key 属性名
 */
export function removeLocal(key: BasicKeys) {
  if (cacheStore.local[BASE_LOCAL_CACHE_KEY]) {
    Reflect.deleteProperty(cacheStore.local[BASE_LOCAL_CACHE_KEY], key)
  }
}

/**
 * @description 清除所有的Local
 */
export function clearLocal() {
  cacheStore.local = {}
  ls.remove(BASE_LOCAL_CACHE_KEY)
}

/**
 * 获取session存储值
 * @param key 属性名
 * @returns null | string
 */
export function getSession(key: BasicKeys) {
  try {
    return cacheStore.session[BASE_SESSION_CACHE_KEY][key]
  } catch (error) {
    return null
  }
}

/**
 * @description 设置Session存储
 * @param key 属性名
 * @param value 属性值
 */
export function setSession(key: BasicKeys, value: BasicStore[BasicKeys]): void {
  const session = ss.get(BASE_SESSION_CACHE_KEY)?.[BASE_SESSION_CACHE_KEY] || {}
  cacheStore.session[BASE_SESSION_CACHE_KEY] =
    {
      ...session,
      ...cacheStore.session[BASE_SESSION_CACHE_KEY]
    } || {}

  cacheStore.session[BASE_SESSION_CACHE_KEY][key] = value
  ss.set(BASE_SESSION_CACHE_KEY, cacheStore.session)
}

/**
 * @description 删除某一个Session存储
 * @param key 属性名
 */
export function removeSession(key: BasicKeys) {
  if (cacheStore.session[BASE_SESSION_CACHE_KEY]) {
    Reflect.deleteProperty(cacheStore.session[BASE_SESSION_CACHE_KEY], key)
  }
}

/**
 * @description 清除所有的Session
 */
export function clearSession() {
  cacheStore.session = {}
  ss.remove(BASE_SESSION_CACHE_KEY)
}
