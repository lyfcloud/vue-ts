import { defineStore } from 'pinia'
import { getAuthCache, setAuthCache } from '@/utils/auth'
import { TOKEN_KEY } from '@/enums/cacheEnum'

interface UserState {
  token?: string
}

export const useUserStore = defineStore({
  id: 'app-user',
  state(): UserState {
    return {
      token: undefined
    }
  },
  getters: {
    getToken(): string {
      return this.token || getAuthCache(TOKEN_KEY)
    }
  },
  actions: {
    setToken(info: string) {
      this.token = info
      setAuthCache(TOKEN_KEY, info)
    }
  }
})
