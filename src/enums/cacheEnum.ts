/*
 * @Author: 李一番
 * @Date: 2021-05-20 21:34:28
 * @Last Modified by: 李一番
 * @Last Modified time: 2021-05-23 18:50:53
 */

// token key
export const TOKEN_KEY = 'TOKEN_'

// 全局local存储key
export const BASE_LOCAL_CACHE_KEY: any = 'LOCAL__CACHE__KEY__'

// 全局sessionl存储key
export const BASE_SESSION_CACHE_KEY: any = 'SESSION__CACHE__KEY__'

export enum CacheTypeEnum {
  SESSION,
  LOCAL
}
