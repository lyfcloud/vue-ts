/*
 * @Author: 李一番
 * @Date: 2021-05-18 18:42:55
 * @Last Modified by: 李一番
 * @Last Modified time: 2021-05-22 18:59:55
 */
/**
 * @description 按需引入antd
 */
import { Button, Icon } from 'ant-design-vue'
import { App } from 'vue'

const components = [Icon, Button]
export function registerAntdComp(app: App) {
  components.forEach((comp: any) => {
    app.component(comp.name || comp.displayName, comp)
  })
}
