/*
 * @Author: 李一番
 * @Date: 2021-05-20 22:03:13
 * @Last Modified by: 李一番
 * @Last Modified time: 2021-05-23 13:50:17
 */

import { CacheTypeEnum } from '@/enums/cacheEnum'
import { ProjectConfig } from '@/types/config'

const setting: ProjectConfig = {
  permissionCacheType: CacheTypeEnum.LOCAL,
  globAppShortName: import.meta.env.VITE_GLOB_APP_SHORT_NAME,
  defaultCacheTime: 60 * 60 * 24 * 7
}

export default setting
