/*
 * @Author: 李一番
 * @Date: 2021-05-22 18:00:30
 * @Last Modified by: 李一番
 * @Last Modified time: 2021-05-22 19:05:44
 */

// 设置AES加密 16个字节
export const cacheCipher = {
  key: '1111111111111111',
  iv: '2222222222222222'
}
