import { createApp } from 'vue'

import { setupRouter } from '@/router'
import { setupStore } from '@/store'
import { registerAntdComp } from '@/antd'
import App from './App.vue'

const app = createApp(App)

// 使用路由
setupRouter(app)

// 使用store
setupStore(app)

// 注册antd组件
registerAntdComp(app)

app.mount('#app')
