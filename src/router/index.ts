/*
 * @Author: 李一番
 * @Date: 2021-05-06 21:44:54
 * @Last Modified by: 李一番
 * @Last Modified time: 2021-05-09 13:45:50
 */

/**
 * 路由
 */
import type { RouteRecordRaw } from 'vue-router'
import type { App } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'

const routes: RouteRecordRaw[] = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/login/index.vue')
  },
  {
    path: '/',
    name: 'Dashboard',
    component: () => import('@/views/dashboard/index.vue')
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

// 配置router
export function setupRouter(app: App<Element>) {
  app.use(router)
}

export default router
