/*
 * @Author: 李一番
 * @Date: 2021-05-06 09:41:41
 * @Last Modified by: 李一番
 * @Last Modified time: 2021-05-06 18:26:52
 */

/**
 * @description 在NODE环境读取env文件的环境变量
 */
export interface ViteEnv {
  VITE_PORT: number
  VITE_GLOB_NAME: string
  VITE_DROP_CONSOLE: boolean
}

/**
 * 读取env环境的配置
 */
export function wrapperEnv(envConfig: any): ViteEnv {
  const ret: any = {}
  for (const envName of Object.keys(envConfig)) {
    ret[envName] = envConfig[envName]
  }
  process.env = { ...process.env, ...envConfig }
  return ret
}
